import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.promo27.entity.Person;
import co.simplon.promo27.repository.Database;
import co.simplon.promo27.repository.PersonRepository;

public class PersonRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    void findAllShouldReturnAListOfPersons() {
        PersonRepository repo = new PersonRepository();
        List<Person> result = repo.findAll();

        assertEquals(4, result.size());
        assertEquals("Amine", result.get(0).getName());
        assertEquals("SIDI IKHLEF", result.get(0).getFirst_name());
        assertEquals(20, result.get(0).getAge());
        assertEquals(1, result.get(0).getId());
        
        assertNotNull(result.get(0).getName());
        assertNotNull(result.get(0).getFirst_name());
        assertNotNull(result.get(0).getAge());
        assertNotNull(result.get(0).getId());
    }

    @Test
    void findByIdWithResult(){
        PersonRepository repo = new PersonRepository();
        Person result = repo.findById(1);

        assertEquals("Amine", result.getName());
        assertEquals("SIDI IKHLEF", result.getFirst_name());
        assertEquals(20, result.getAge());
        assertEquals(1, result.getId());
    }

    @Test
    void findByIdNoResult(){
        PersonRepository repo = new PersonRepository();
        Person result = repo.findById(10000);
        assertNull(result);

    }

    @Test
    void deleteSuccess(){
        PersonRepository repo = new PersonRepository();
        repo.delete(1);
        assertEquals(null, repo.findById(1));
    }

}
