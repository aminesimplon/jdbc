package co.simplon.promo27.entity;

public class Person {
    private int id;
    private String name;
    private String first_name;
    private int age;

    public Person(String name, String first_name, int age) {
        this.name = name;
        this.first_name = first_name;
        this.age = age;
    }

    public Person(int id, String name, String first_name, int age) {
        this.id = id;
        this.name = name;
        this.first_name = first_name;
        this.age = age;
    }

    public Person() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
