package co.simplon.promo27;

import java.util.List;

import co.simplon.promo27.entity.Person;
import co.simplon.promo27.repository.PersonRepository;

public class Main {
    public static void main(String[] args) {

        PersonRepository repo = new PersonRepository();
        List<Person> persons = repo.findAll();
        System.out.println(persons.size());

        Person Aymeric =  repo.findById(1);
        Aymeric.setName("Aymeric");
        Aymeric.setFirst_name("Aymeric");
        Aymeric.setAge(20);



        if (repo.updatePerson(Aymeric)) {
            System.out.println("Updated");
        }else{
            System.out.println("Not updated");
        }
    }
}