package co.simplon.promo27.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.promo27.entity.Person;

public class PersonRepository {
    public List<Person> findAll() {
        List<Person> list = new ArrayList<>();

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person");

            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                Person person = new Person(result.getInt("id"), result.getString("name"),
                        result.getString("first_name"), result.getInt("age"));
                list.add(person);
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }

        return list;
    }

    public Person findById(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM person where id =?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            if (result.next()) {
                return new Person(result.getInt("id"), result.getString("name"), result.getString("first_name"),
                        result.getInt("age"));
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM person where id =?");
            stmt.setInt(1, id);
            int result = stmt.executeUpdate();
            if (result > 0) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

    public boolean persist(Person person) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO person(name, first_name, age) VALUES (?, ?, ?)");
            stmt.setString(1, person.getName());
            stmt.setString(2, person.getFirst_name());
            stmt.setInt(3, person.getAge());

            if (stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                person.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updatePerson(Person person) {
            try (Connection connection = Database.connect()) {
                PreparedStatement stmt = connection.prepareStatement("UPDATE person SET name = ?, first_name = ?, age = ? WHERE id = ?");
                stmt.setString(1, person.getName());
                stmt.setString(2, person.getFirst_name());
                stmt.setInt(3, person.getAge());
                stmt.setInt(4, person.getId());

                if (stmt.executeUpdate() == 1) {
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        return false;   
    }

}
