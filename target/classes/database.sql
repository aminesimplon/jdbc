-- Active: 1709545637393@@127.0.0.1@3306@p27_jdbc

DROP TABLE IF EXISTS person;

CREATE TABLE person ( 
    id INT PRIMARY KEY AUTO_INCREMENT, 
    name VARCHAR(64) NOT NULL,
    first_name VARCHAR(64) NOT NULL,
    age INT
);

INSERT INTO person(name, first_name, age) VALUES
('Amine', 'SIDI IKHLEF', 20),
('Vero', 'Vero', 21),
('Ismael', 'Brice', 32),
('El-famoso', 'SIDI IKHLEF', 50);


SELECT * FROM person;